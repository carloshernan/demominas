# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/3.1.0/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/3.1.0/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/3.1.0/reference/htmlsingle/#web)
* [Thymeleaf](https://docs.spring.io/spring-boot/docs/3.1.0/reference/htmlsingle/#web.servlet.spring-mvc.template-engines)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/3.1.0/reference/htmlsingle/#data.sql.jpa-and-spring-data)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/3.1.0/reference/htmlsingle/#using.devtools)

### Guides

The following guides illustrate how to use some features concretely:

* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)
* [Handling Form Submission](https://spring.io/guides/gs/handling-form-submission/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)

### Install con docker

* Tener instalado Docker
* Generar imagen Docker para ello pararse en la carpeta donde se encuentra el archivo Dockerfile y correr el siguiente comando:
* docker build -t demominas .
* docker run -dp 8080:8080 demominas
* abrir navegador e ingresar la siguiente url http://localhost:8080/
* para detener el contenedor docker stop idcontainer
* para matar el contenedor en caso de ser necesario docker exec -it idcontainer kill 1

### Install sin docker

* Tener instalado Java 17
* abrir una consola y ubicar la carpeta donde se encuentra el archivo demoMinas-0.0.1-SNAPSHOT.jar
* ejecutar en la consola el siguiente comando: java -jar demoMinas-0.0.1-SNAPSHOT.jar
* abrir el navegador e ingresar la siguiente url http://localhost:8080


### Ingreso de la aplicación

* En la página de login ingresar con el usuario operador.
username=operador
password=operador

* favor parametrizar los topes de minerales ingresando por el botn de topes
* A continuación ingresar con el usuario explotador o con el usuario comecializador para registar las solicitudes.
username=ComercioUno
password=explotador

username=ComercioDos
password=comercializador

* para aprobar las solicitudes ingresar con el usuario operador o autorizador
username=autorizador
password=autorizador

### Tecnologias utilizadas

* Sprint boot versión 3.1.0
* Maven
* Java 17
* jakarta EE
* Thymeleaf
* Bootstrap 5.3.0
* Sprint security
* lombok
* Base de datos h2 
* La base de ddatos está configurada en memoria si se desea se puede configurar con archivo en disco usando la propiedad spring.datasource.url del archivo    application.properties que se encuentra en resources.
* JPA

### Arquitectura MVC (Modelo-Vista Controlador) Multicapa.

* Usando Sprint boot componentes @Controller
* Capa de servicios usando los componentes @Service
* Capara de persistencia usando interfaces de repositorio para acceder a cada @Entity
* Archivos de mensajeria messages.properties e internacionalización español e ingles.
* Parametrización de datos en el archivo data.sql












