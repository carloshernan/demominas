package com.example.demominas.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.example.demominas.entity.Explotacion;

public interface IExplotacionRepositorio extends JpaRepository<Explotacion ,Long> {
	
	@Query("select ex from Explotacion ex where ex.comercio.idComercio = :idComercio")
	public List<Explotacion> encontrarExplotacionByComercio(@Param("idComercio") Long idComercio);

}
