package com.example.demominas.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.example.demominas.entity.TopesMineral;

public interface ITopesMineralRepositorio extends JpaRepository<TopesMineral ,Long> {
	
	@Query("select t from TopesMineral t where t.mineral.idMineral = :idMineral")
	public List<TopesMineral> encontrarTopesMIneralByIdMineral(@Param("idMineral") Long idMineral);
	
	@Query(value = "SELECT max(idTopeMineral) FROM TopesMineral")
    public Long getMaxId();

}
