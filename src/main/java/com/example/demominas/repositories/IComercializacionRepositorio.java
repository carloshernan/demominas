package com.example.demominas.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demominas.entity.Comercializacion;

public interface IComercializacionRepositorio extends JpaRepository<Comercializacion ,Long> {
	
	@Query("select comer from Comercializacion comer where comer.comercio.idComercio = :idComercio")
	public List<Comercializacion> encontrarComercializacionByComercio(@Param("idComercio") Long idComercio);

}
