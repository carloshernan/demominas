package com.example.demominas.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.example.demominas.entity.Comercio;

public interface IComercioRepositorio extends JpaRepository<Comercio ,Long> {
	
	@Query("select c from Comercio c where c.tipoUsuario = :idTipoUsuario")
	public List<Comercio> encontrarComercioByTipoUsuario(@Param("idTipoUsuario") Integer idTipoUusuario);

}
