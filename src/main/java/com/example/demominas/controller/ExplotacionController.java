package com.example.demominas.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import com.example.demominas.entity.Comercio;
import com.example.demominas.entity.Explotacion;
import com.example.demominas.entity.Mineral;
import com.example.demominas.services.IComercioService;
import com.example.demominas.services.IExplotacionService;
import com.example.demominas.services.ITopesMineralService;

import jakarta.validation.Valid;

@Controller
public class ExplotacionController {
		
	private IExplotacionService explotaservice;
	
	private IComercioService comercioservice;
	
	private ITopesMineralService topesservice;

	public ExplotacionController(ITopesMineralService topesservice, IComercioService comercioservice,
							  IExplotacionService explotaservice) {
		this.topesservice = topesservice;
		this.comercioservice = comercioservice;
		this.explotaservice = explotaservice;
	}
	
			
	@PostMapping("/guardarExplotacion")
	public String guardarExplotacion(@Valid Comercio comercio, Errors errores) {
		if(errores.hasErrors()) {
			return "agregarExplotacion";
		}
		Explotacion explota=new Explotacion();
		Mineral mineral= new Mineral();
		mineral.setIdMineral(comercio.getExplotacion().getMineral().getIdMineral());
		
		Double topeparam=topesservice.encontrarTopesMineralByIdMineral(mineral.getIdMineral()).get(0).getCantidadTope();
		explota.setCantidadMineral(comercio.getExplotacion().getCantidadMineral());
	
		if(explota.getCantidadMineral()<=topeparam) {
			explota.setMineral(mineral);
			explota.setIdEstadoExplotacion(1);
			explota.setComercio(comercio);
			explotaservice.guardar(explota);
			return "redirect:/";
		}else {
			return "errortopes";
		}
	}
	
	@GetMapping("/agregarExplotacion")
	public String agregarExplotacion(Comercio comercio, Model model) {
		model.addAttribute("comercio",comercio);
		return "agregarExplotacion";
	}
	
		
	@GetMapping("/eliminarExplotacion")
	public String eliminar(Explotacion explotacion,Comercio comercio, Model model) {
		explotaservice.eliminar(explotacion);
		return "redirect:/";
	}
	
	@GetMapping("/editarExplotacion")
	public String editarExplotacion(Explotacion explotacion,Comercio comercio, Model model) {
		explotacion=explotaservice.encontrarExplotacion(explotacion);
		comercio=comercioservice.encontrarComercio(explotacion.getComercio());
		comercio.setExplotacion(explotacion);
		model.addAttribute("comercio",comercio);
		model.addAttribute("explotacion",explotacion);
		return "modificarExplotacion";
	}
	
	@PostMapping("/guardarEdicionExplotacion")
	public String guardarEdicionExplotacion(@Valid Comercio comercio,Explotacion explotacion, Errors errores) {
		if(errores.hasErrors()) {
			return "agregarExplotacion";
		}
		explotacion=explotaservice.encontrarExplotacion(comercio.getExplotacion());
		explotacion.setCantidadMineral(comercio.getExplotacion().getCantidadMineral());
		explotacion.setMineral(comercio.getExplotacion().getMineral());
		explotaservice.guardar(explotacion);
		return "redirect:/";
	}
	
	@GetMapping("/aprobarExplotacion")
	public String aprobar(Explotacion explotacion, Model model) {
		explotacion=explotaservice.encontrarExplotacion(explotacion);
		explotacion.setIdEstadoExplotacion(2);
		explotaservice.guardar(explotacion);
		return "redirect:/";
	}
	
	@GetMapping("/reprobarExplotacion")
	public String reprobar(Explotacion explotacion, Model model) {
		explotacion=explotaservice.encontrarExplotacion(explotacion);
		explotacion.setIdEstadoExplotacion(3);
		explotaservice.guardar(explotacion);
		return "redirect:/";
	}
	

}
