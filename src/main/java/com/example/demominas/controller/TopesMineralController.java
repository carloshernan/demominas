package com.example.demominas.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import com.example.demominas.entity.Comercio;
import com.example.demominas.entity.TopesMineral;
import com.example.demominas.services.ITopesMineralService;

import jakarta.validation.Valid;

@Controller
public class TopesMineralController {
		
	private ITopesMineralService topesservice;

	public TopesMineralController(ITopesMineralService topesservice) {
				this.topesservice = topesservice;
	}
	
	@PostMapping("/guardarTope")
	public String guardar(@Valid TopesMineral tope, Errors errores) {
		if(errores.hasErrors()) {
			return "topesMinerales";
		}
		topesservice.guardar(tope);
		return "redirect:/";
	}
			
	
	@GetMapping("/editarTope")
	public String editarTope(TopesMineral tope,Comercio comercio, Model model) {
		tope=topesservice.encontrarTopesMineral(tope);
		model.addAttribute("tope",tope);
		return "modificarTope";
	}
	
	@PostMapping("/guardarEdicionTope")
	public String guardarEdicionTope(@Valid Comercio comercio,TopesMineral tope, Errors errores) {
		if(errores.hasErrors()) {
			return "agregarTope";
		}
		topesservice.guardar(tope);
		return "redirect:/";
	}
	
	@GetMapping("/eliminarTope")
	public String eliminar(TopesMineral tope,Comercio comercio, Model model) {
		topesservice.eliminar(tope);
		return "redirect:/";
	}
	
	@GetMapping("/topes")
	public String topesMineral( Model model) {
		List<TopesMineral> topes=topesservice.listarTopesMineral();
		model.addAttribute("topes",topes);
		return "topesMinerales";
	}
	
}
