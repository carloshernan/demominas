package com.example.demominas.controller;

import java.util.Date;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import com.example.demominas.entity.Comercio;
import com.example.demominas.entity.Explotacion;
import com.example.demominas.entity.Comercializacion;
import com.example.demominas.services.IComercioService;
import com.example.demominas.services.IExplotacionService;
import com.example.demominas.services.IComercializacionService;

import jakarta.validation.Valid;

@Controller
public class ComercioController {
		
	private IComercioService comercioservice;
	
	private IExplotacionService explotaservice;
	
	private IComercializacionService comercializacionservice;


	public ComercioController(IComercializacionService comercializacionservice, IComercioService comercioservice,
							  IExplotacionService explotaservice) {
		this.comercializacionservice = comercializacionservice;
		this.comercioservice = comercioservice;
		this.explotaservice = explotaservice;
	}
		
	@GetMapping("/")
	public String inicio(Model model, @AuthenticationPrincipal User user) {
		var roluser="";
		Integer tipoUsuario=0;
		for (GrantedAuthority ga : user.getAuthorities()) {
		      roluser=ga.getAuthority();
		}
		if(roluser.equals("ROLE_EXPLOTADOR")) {
			tipoUsuario=1;
		}else if(roluser.equals("ROLE_COMERCIALIZADOR")) {
			tipoUsuario=2;
		}
		var comercios=comercioservice.listarComerciosByTipoUsuario(tipoUsuario);
		var comerciostodos=comercioservice.listarComercios();
		if(tipoUsuario==0) {
			model.addAttribute("comercios",comerciostodos);
		}else {
			model.addAttribute("comercios",comercios);
		}
		return "index";
	}
		
	@GetMapping("/agregar")
	public String agregar(Comercio comercio) {
		return "modificar";
	}
	
	@PostMapping("/guardar")
	public String guardar(@Valid Comercio comercio, Errors errores) {
		if(errores.hasErrors()) {
			return "modificarComercio";
		}
		comercio.setIdEstadoInscripcion(1);
		comercio.setIdEstadoRegistro(1);
		comercio.setFechaEstadoInscripcion(new Date());
		comercio.setFechaRegistro(new Date());
		comercio.setFechaEstadoRegistro(new Date());
		comercioservice.guardar(comercio);
		return "redirect:/";
	}
	
	@GetMapping("/editar")
	public String editar(Comercio comercio, Model model) {
		comercio=comercioservice.encontrarComercio(comercio);
		List<Explotacion> explotaciones=explotaservice.encontrarExplotacionByIdComercio(comercio.getIdComercio());
		List<Comercializacion> comercializaciones=comercializacionservice.encontrarComercializacionByIdComercio(comercio.getIdComercio());
		
		if(!explotaciones.isEmpty() && explotaciones !=null ) {
			model.addAttribute("explotaciones",explotaciones);
		}
		if(!comercializaciones.isEmpty() && comercializaciones !=null ) {
			model.addAttribute("comercializaciones",comercializaciones);
		}
		model.addAttribute("comercio",comercio);
		return "modificarComercio";
	}
	
		
	@GetMapping("/eliminar")
	public String eliminar(Comercio comercio, Model model) {
		List<Explotacion> explot=explotaservice.encontrarExplotacionByIdComercio(comercio.getIdComercio());
		if(explot.isEmpty() || explot == null) {
			comercioservice.eliminar(comercio);
		}
		return "redirect:/";
	}
	
	@GetMapping("/aprobar")
	public String aprobar(Comercio comercio, Model model) {
		comercio=comercioservice.encontrarComercio(comercio);
		comercio.setIdEstadoInscripcion(2);
		comercio.setIdEstadoRegistro(2);
		comercio.setFechaEstadoInscripcion(new Date());
		comercio.setFechaEstadoRegistro(new Date());
		comercioservice.guardar(comercio);
		return "redirect:/";
	}
	
	@GetMapping("/reprobar")
	public String reprobar(Comercio comercio, Model model) {
		comercio=comercioservice.encontrarComercio(comercio);
		comercio.setIdEstadoInscripcion(3);
		comercio.setIdEstadoRegistro(2);
		comercio.setFechaEstadoInscripcion(new Date());
		comercio.setFechaEstadoRegistro(new Date());
		comercioservice.guardar(comercio);
		return "redirect:/";
	}

}
