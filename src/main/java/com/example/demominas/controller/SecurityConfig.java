package com.example.demominas.controller;

import org.springframework.context.annotation.Bean;



import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
		
		@Bean
		public InMemoryUserDetailsManager userDetailsService() {
		    UserDetails user = User.builder().username("user")
		            .password(passwordEncoder().encode("user"))
		            .roles("USER")
		            .build();
		    
		    UserDetails admin = User.builder().username("admin")
		    		.password(passwordEncoder().encode("admin"))
	                .roles("ADMIN")
	                .build();
		    
		    UserDetails autorizador = User.builder().username("autorizador")
		    		.password(passwordEncoder().encode("autorizador"))
	                .roles("AUTORIZADOR")
	                .build();
		    
		    UserDetails operador = User.builder().username("operador")
		    		.password(passwordEncoder().encode("operador"))
	                .roles("OPERADOR")
	                .build();
		    
		    UserDetails explotador = User.builder().username("ComercioUno")
		    		.password(passwordEncoder().encode("explotador"))
	                .roles("EXPLOTADOR")
	                .build();
		    
		    UserDetails comercializador = User.builder().username("ComercioDos")
		    		.password(passwordEncoder().encode("comercializador"))
	                .roles("COMERCIALIZADOR")
	                .build();
		    
		    return new InMemoryUserDetailsManager(user,admin,autorizador,operador,explotador,comercializador);
		}
		
		@Bean
		public PasswordEncoder passwordEncoder() {
		    return new BCryptPasswordEncoder();
		}
		
		
		@Bean
	    public SecurityFilterChain configure(HttpSecurity http) throws Exception {
	        
			return http.csrf().disable()
					.authorizeRequests()
					.requestMatchers("/editar/**","/agregar/**","/eliminar").hasAnyRole("ADMIN","EXPLOTADOR","COMERCIALIZADOR","OPERADOR","AUTORIZADOR")
					.requestMatchers("/agregarExplotacion/**").hasRole("EXPLOTADOR")
					.requestMatchers("/agregarComercializacion/**").hasRole("COMERCIALIZADOR")
					.requestMatchers("/").hasAnyRole("USER","ADMIN","EXPLOTADOR","COMERCIALIZADOR","OPERADOR","AUTORIZADOR")
					//.requestMatchers("/h2-console/**").permitAll()
					.and()
                    .formLogin().loginPage("/login")
                    .and().exceptionHandling().accessDeniedPage("/errores/403")
                    .and().httpBasic().and().build();
	    }


}
