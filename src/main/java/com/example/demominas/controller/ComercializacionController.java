package com.example.demominas.controller;

import java.util.Date;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import com.example.demominas.entity.Comercializacion;
import com.example.demominas.entity.Comercio;
import com.example.demominas.entity.Mineral;
import com.example.demominas.services.IComercializacionService;
import com.example.demominas.services.IComercioService;
import com.example.demominas.services.ITopesMineralService;
import jakarta.validation.Valid;

@Controller
public class ComercializacionController {
		
	private IComercializacionService comercializacionservice;

	private IComercioService comercioservice;
	
	private ITopesMineralService topesservice;

	public ComercializacionController(IComercializacionService comercializacionservice,IComercioService comercioservice,ITopesMineralService topesservice) {
		this.comercializacionservice = comercializacionservice;
		this.comercioservice = comercioservice;
		this.topesservice = topesservice;
	}




	@PostMapping("/guardarComer")
	public String guardarComercializacion(@Valid Comercio comercio, Errors errores) {
		if(errores.hasErrors()) {
			return "agregarComercializacion";
		}
		Comercializacion comer=new Comercializacion();
		Mineral mineral= new Mineral();
		mineral.setIdMineral(comercio.getComercializacion().getMineral().getIdMineral());
		Double topeparam=topesservice.encontrarTopesMineralByIdMineral(mineral.getIdMineral()).get(0).getCantidadTope();
		comer.setCantidadMineral(comercio.getComercializacion().getCantidadMineral());
		if(comer.getCantidadMineral()<=topeparam) {
			comer.setMineral(mineral);
			comer.setIdEstadoComercializacion(1);
			comer.setFechaEstadoComercializacion(new Date());
			comer.setIdEstadoTransaccion(1);
			comer.setFechaEstadoTransaccion(new Date());
			comer.setComercio(comercio);
			comercializacionservice.guardar(comer);
			return "redirect:/";
			
		}else {
			
			return "errortopes";
		}
	}
	
	@GetMapping("/agregarComer")
	public String agregarComercializacion(Comercio comercio, Model model) {
		model.addAttribute("comercio",comercio);
		return "agregarComercializacion";
	}
			
	@GetMapping("/eliminarComer")
	public String eliminar(Comercializacion comercioalizacion, Comercio comercio,Model model) {
		comercializacionservice.eliminar(comercioalizacion);
		return "redirect:/";
	}
	
	@GetMapping("/editarComer")
	public String editarExplotacion(Comercializacion comercializacion,Comercio comercio, Model model) {
		comercializacion=comercializacionservice.encontrarComercializacion(comercializacion);
		comercio=comercioservice.encontrarComercio(comercializacion.getComercio());
		comercio.setComercializacion(comercializacion);
		model.addAttribute("comercio",comercio);
		model.addAttribute("comercializacion",comercializacion);
		return "modificarComercializacion";
		
	}
	
	@PostMapping("/guardarEdicionComercializacion")
	public String guardarEdicionComercializacion(@Valid Comercio comercio,Comercializacion comercializacion, Errors errores) {
		if(errores.hasErrors()) {
			return "agregarComercializacion";
		}
		comercializacion=comercializacionservice.encontrarComercializacion(comercio.getComercializacion());
		comercializacion.setCantidadMineral(comercio.getComercializacion().getCantidadMineral());
		comercializacion.setMineral(comercio.getComercializacion().getMineral());
		comercializacionservice.guardar(comercializacion);
		return "redirect:/";
	}
	
	@GetMapping("/aprobarComer")
	public String aprobar(Comercializacion comercializacion, Model model) {
		comercializacion=comercializacionservice.encontrarComercializacion(comercializacion);
		comercializacion.setIdEstadoComercializacion(2);
		comercializacionservice.guardar(comercializacion);
		return "redirect:/";
	}
	
	@GetMapping("/reprobarComer")
	public String reprobar(Comercializacion comercializacion, Model model) {
		comercializacion=comercializacionservice.encontrarComercializacion(comercializacion);
		comercializacion.setIdEstadoComercializacion(3);
		comercializacionservice.guardar(comercializacion);
		return "redirect:/";
	}

}
