package com.example.demominas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.example.*")
@EntityScan("com.example.*")

public class DemoMinasApplication {

	public static void main(String[] args) {

		SpringApplication.run(DemoMinasApplication.class, args	);
	}

}
