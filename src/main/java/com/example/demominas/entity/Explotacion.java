package com.example.demominas.entity;

import java.io.Serializable;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@Entity
@Table
@AllArgsConstructor
@NoArgsConstructor
public class Explotacion implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idExplotacion;
		
	private Double cantidadMineral;
	
	private Double valor;
	
	private Integer idEstadoExplotacion;
	
	@ManyToOne
	@JoinColumn(name="idComercio")
	private Comercio comercio;
	
	@ManyToOne
	@JoinColumn(name="idMineral")
	private Mineral mineral;
	
}
