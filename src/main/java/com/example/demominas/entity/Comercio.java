package com.example.demominas.entity;
import java.io.Serializable;
import java.util.Date;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@Entity
@Table
@AllArgsConstructor
@NoArgsConstructor
public class Comercio implements Serializable{
		
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idComercio;
		
	private String nombre;
		
	private Integer tipoDocumento;
	
	private String documento;
			
	private Date fechaRegistro;
		
	private Integer tipoUsuario;
	
	private String departamento;
	
	private String municipio;
		
	private Integer idEstadoInscripcion;
	
	private Date fechaEstadoInscripcion;
	
	private Integer idEstadoRegistro;
	
	private Date fechaEstadoRegistro;
	
	private Explotacion explotacion;
	
	private Comercializacion comercializacion;
	
}
