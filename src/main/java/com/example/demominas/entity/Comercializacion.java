package com.example.demominas.entity;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@Entity
@Table
@AllArgsConstructor
@NoArgsConstructor
public class Comercializacion implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idComercializacion;
		
	private Double cantidadMineral;
	
	private Double valor;
	
	private Integer idEstadoTransaccion;
	
	@Temporal(TemporalType.DATE)
	private Date fechaEstadoTransaccion;
	
	private Integer idTipoComercializacion;
		
	private Integer idEstadoComercializacion;
	
	@Temporal(TemporalType.DATE)
	private Date fechaEstadoComercializacion;
	
	@ManyToOne
	@JoinColumn(name="idComercio")
	private Comercio comercio;
	
	@ManyToOne
	@JoinColumn(name="idMineral")
	private Mineral mineral;
}
