package com.example.demominas.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.example.demominas.entity.Comercializacion;
import com.example.demominas.repositories.IComercializacionRepositorio;

@Service
public class ComercializacionServiceImpl implements IComercializacionService {
		
	private IComercializacionRepositorio comercialirepositorio;

	public ComercializacionServiceImpl(IComercializacionRepositorio comercialirepositorio){
		this.comercialirepositorio=comercialirepositorio;
	}
	
	@Override
	@Transactional(readOnly=true)
	public List<Comercializacion> listarComercializaciones() {
		return comercialirepositorio.findAll();
	}

	@Override
	@Transactional
	public void guardar(Comercializacion comer) {
		comercialirepositorio.save(comer);
	}

	@Override
	@Transactional
	public void eliminar(Comercializacion comer) {
		comercialirepositorio.delete(comer);
	}

	@Override
	@Transactional(readOnly=true)
	public Comercializacion encontrarComercializacion(Comercializacion comer) {
		return comercialirepositorio.findById(comer.getIdComercializacion()).orElse(null);
	}

	@Override
	public List<Comercializacion> encontrarComercializacionByIdComercio(Long idComercio) {
		// TODO Auto-generated method stub
		return comercialirepositorio.encontrarComercializacionByComercio(idComercio);
	}

}
