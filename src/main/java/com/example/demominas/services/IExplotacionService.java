package com.example.demominas.services;

import java.util.*;

import org.springframework.stereotype.Component;
import com.example.demominas.entity.Explotacion;

@Component
public interface IExplotacionService {
	
	public List<Explotacion> listarExplotaciones();
	
	public void guardar(Explotacion explota);
	
	public void eliminar(Explotacion explota);
	
	public Explotacion encontrarExplotacion(Explotacion explota);
	
	public List<Explotacion> encontrarExplotacionByIdComercio(Long idComercio);
	
	
	
	
	
}
