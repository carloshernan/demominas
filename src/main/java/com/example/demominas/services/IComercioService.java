package com.example.demominas.services;

import java.util.*;

import org.springframework.stereotype.Component;

import com.example.demominas.entity.Comercio;

@Component
public interface IComercioService {
	
	public List<Comercio> listarComercios();
	
	public void guardar(Comercio comercio);
	
	public void eliminar(Comercio comercio);
	
	public Comercio encontrarComercio(Comercio comercio);
	
	public List<Comercio> listarComerciosByTipoUsuario(Integer idTipoUsuario);
	
}
