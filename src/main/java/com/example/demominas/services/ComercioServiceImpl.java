package com.example.demominas.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demominas.entity.Comercio;
import com.example.demominas.repositories.IComercioRepositorio;

@Service
public class ComercioServiceImpl implements IComercioService {
		
	private IComercioRepositorio comerciorepositorio;

	public ComercioServiceImpl(IComercioRepositorio comerciorepositorio){
		this.comerciorepositorio=comerciorepositorio;
	}
	
	@Override
	@Transactional(readOnly=true)
	public List<Comercio> listarComercios() {
		return comerciorepositorio.findAll();
	}

	@Override
	@Transactional
	public void guardar(Comercio comercio) {
		comerciorepositorio.save(comercio);
	}

	@Override
	@Transactional
	public void eliminar(Comercio comercio) {
		comerciorepositorio.delete(comercio);
	}

	@Override
	@Transactional(readOnly=true)
	public Comercio encontrarComercio(Comercio comercio) {
		return comerciorepositorio.findById(comercio.getIdComercio()).orElse(null);
	}

	@Override
	public List<Comercio> listarComerciosByTipoUsuario(Integer idTipoUsuario) {
		// TODO Auto-generated method stub
		return comerciorepositorio.encontrarComercioByTipoUsuario(idTipoUsuario);
	}

}
