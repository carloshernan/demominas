package com.example.demominas.services;

import java.util.List;
import org.springframework.stereotype.Component;
import com.example.demominas.entity.Comercializacion;

@Component
public interface IComercializacionService {
	
	public List<Comercializacion> listarComercializaciones();
	
	public void guardar(Comercializacion comer);
	
	public void eliminar(Comercializacion comer);
	
	public Comercializacion encontrarComercializacion(Comercializacion comer);
	
	public List<Comercializacion> encontrarComercializacionByIdComercio(Long idComercio);
	
}
