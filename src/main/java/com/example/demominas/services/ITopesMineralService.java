package com.example.demominas.services;

import java.util.List;
import org.springframework.stereotype.Component;
import com.example.demominas.entity.TopesMineral;

@Component
public interface ITopesMineralService {
	
	public List<TopesMineral> listarTopesMineral();
	
	public void guardar(TopesMineral tope);
	
	public void eliminar(TopesMineral tope);
	
	public TopesMineral encontrarTopesMineral(TopesMineral tope);
	
	public List<TopesMineral> encontrarTopesMineralByIdMineral(Long idMineral);
	
	public Long maxIdTopesMineral();
	
}
