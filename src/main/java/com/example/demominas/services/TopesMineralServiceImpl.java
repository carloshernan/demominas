package com.example.demominas.services;

import java.util.List;

import com.example.demominas.repositories.IExplotacionRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.example.demominas.entity.TopesMineral;
import com.example.demominas.repositories.ITopesMineralRepositorio;

@Service
public class TopesMineralServiceImpl implements ITopesMineralService {
		
	ITopesMineralRepositorio topesrepositorio;

	public TopesMineralServiceImpl(ITopesMineralRepositorio topesrepositorio){
		this.topesrepositorio=topesrepositorio;
	}
	
	@Override
	@Transactional(readOnly=true)
	public List<TopesMineral> listarTopesMineral() {
		return topesrepositorio.findAll();
	}

	@Override
	@Transactional
	public void guardar(TopesMineral tope) {
		topesrepositorio.save(tope);
	}

	@Override
	@Transactional
	public void eliminar(TopesMineral tope) {
		topesrepositorio.delete(tope);
	}

	@Override
	@Transactional(readOnly=true)
	public TopesMineral encontrarTopesMineral(TopesMineral tope) {
		return topesrepositorio.findById(tope.getIdTopeMineral()).orElse(null);
	}

	@Override
	@Transactional(readOnly=true)
	public List<TopesMineral> encontrarTopesMineralByIdMineral(Long idMineral) {
		return topesrepositorio.encontrarTopesMIneralByIdMineral(idMineral);
	}

	@Override
	public Long maxIdTopesMineral() {
		return topesrepositorio.getMaxId();
	}



}
