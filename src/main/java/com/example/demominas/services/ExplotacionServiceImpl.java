package com.example.demominas.services;

import java.util.List;

import com.example.demominas.repositories.IComercioRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Query;
import com.example.demominas.entity.Comercio;
import com.example.demominas.entity.Explotacion;
import com.example.demominas.repositories.IExplotacionRepositorio;

@Service
public class ExplotacionServiceImpl implements IExplotacionService {

	private IExplotacionRepositorio explotarepositorio;

	public ExplotacionServiceImpl(IExplotacionRepositorio explotarepositorio){
		this.explotarepositorio=explotarepositorio;
	}


	@Override
	@Transactional(readOnly=true)
	public List<Explotacion> listarExplotaciones() {
		return explotarepositorio.findAll();
	}

	@Override
	@Transactional
	public void guardar(Explotacion explota) {
		explotarepositorio.save(explota);
	}

	@Override
	@Transactional
	public void eliminar(Explotacion explota) {
		explotarepositorio.delete(explota);
	}

	@Override
	@Transactional(readOnly=true)
	public Explotacion encontrarExplotacion(Explotacion explota) {
		return explotarepositorio.findById(explota.getIdExplotacion()).orElse(null);
	}

	@Override
	public List<Explotacion> encontrarExplotacionByIdComercio(Long idComercio) {
		// TODO Auto-generated method stub
		return explotarepositorio.encontrarExplotacionByComercio(idComercio);
	}



}
